<?php

require_once 'Vendor/tpl.php';
require_once 'request.php';
require_once 'Person.php';


$people = [];

$request = new Request($_REQUEST);


$command = $request->param('command')
    ? $request->param('command')
    : 'show_list_page';

if ($command === 'show_list_page') {
    $data = [
        'title' => 'List page',
        'template' => 'list.html',
        'command' => 'show_list_page',
    ];
    print renderTemplate('templates/main.html', $data);

} else if ($command === 'show_add_page') {

    $data = [
        'title' => 'Add new entry',
        'template' => 'add.html',
        'command' => 'add_list_page'
    ];

    print renderTemplate('templates/main.html', $data);

} else if ($command === 'add_list_page') {
    $firstName = ($request->param('firstName'));
    $lastName = ($request->param('lastName'));
    $phone = ($request->param('phone'));

    print $firstName;

    $people[] = new Person($firstName, $lastName, $phone);

    $data = [
        'template' => 'list.html',
        'command' => 'show_list_page'
    ];

    print renderTemplate('templates/main.html', $data);
}